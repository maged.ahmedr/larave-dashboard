### installation steps:

```
git clone https://gitlab.com/maged.ahmedr/larave-dashboard.git
cd intro project
cp .env.example .env
php artisan key:generate
composer install
```
Change db credentials then

```
php artisan migrate --seed
```

Dommy data 

Admin

- email: `admin@ibtdi.com`
- password: `password`

Customer

- email: `customer@demo.com`
- password: `password`
