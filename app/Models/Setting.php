<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Laraeast\LaravelSettings\Models\Setting as SettingModel;
use AhmedAliraqi\LaravelMediaUploader\Entities\Concerns\HasUploader;

class Setting extends SettingModel implements HasMedia
{
    use HasUploader,
        InteractsWithMedia;
}
