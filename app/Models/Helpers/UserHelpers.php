<?php

namespace App\Models\Helpers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;

trait UserHelpers
{
    /**
     * Determine whether the user type is admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->type == User::ADMIN_TYPE;
    }

    /**
     * Determine whether the user type is customer.
     *
     * @return bool
     */
    public function isCustomer()
    {
        return $this->type == User::CUSTOMER_TYPE;
    }

    /**
     * Determine whether the user is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Set the user type.
     *
     * @return $this
     */
    public function setType($type)
    {
        if (Gate::allows('updateType', $this)
            && in_array($type, array_keys(trans('users.types')))) {
            $this->forceFill([
                'type' => $type,
            ])->save();
        }

        return $this;
    }

    /**
     * get the user type.
     *
     * @return $this
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function canAccessDashboard()
    {
        $manager = app('impersonate');

        // TRUE if your are impersonating an user.
        if ($manager->isImpersonating()) {
            return true;
        }

        if (($this->getType() != 'customer' && $this->owner_id == null && $this->isActive()) || $this->isAdmin()) {
            return true;
        }

        return false;
    }

    /**
     * The user profile image url.
     *
     * @return bool
     */
    public function getAvatar()
    {
        return $this->getFirstMediaUrl('avatars');
    }

    /**
     * The user ID image url.
     *
     * @return bool
     */
    public function getId()
    {
        return $this->getFirstMediaUrl('ids');
    }
}
