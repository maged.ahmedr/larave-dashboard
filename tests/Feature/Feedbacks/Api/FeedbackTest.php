<?php

namespace Tests\Feature\Feedbacks\Api;

use Tests\TestCase;
use App\Models\Feedback;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedbackTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function customer_can_send_feedback_message()
    {
        $this->actingAsCustomer();

        $this->assertEquals(0, Feedback::count());

        $response = $this->postJson(
            route('feedback.store'),
            [
                'title' => 'some problem',
                'message' => 'some feedback',
            ]
        );

        $response->assertSuccessful();

        $this->assertEquals(
            $response->json('message'),
            trans('feedback.messages.sent')
        );

        $this->assertEquals(1, Feedback::count());

        $this->assertDatabaseHas('feedback', [
            'title' => 'some problem',
            'message' => 'some feedback',
        ]);
    }
}
