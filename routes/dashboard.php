<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LocaleController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Accounts\Dashboard\UserController;
use App\Http\Controllers\Accounts\Dashboard\AdminController;
use App\Http\Controllers\Reports\Dashboard\ReportController;
use App\Http\Controllers\Profile\Dashboard\ProfileController;
use App\Http\Controllers\Settings\Dashboard\SettingController;
use App\Http\Controllers\Accounts\Dashboard\CustomerController;
use App\Http\Controllers\Blogs\BlogController;
use App\Http\Controllers\Feedbacks\Dashboard\FeedbackController;
use App\Http\Controllers\Projects\ProjectController;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('locale/{locale}', [LocaleController::class, 'update'])
    ->name('locale')
    ->where('locale', '(ar|en)');

Route::get('/', [DashboardController::class, 'index'])->name('home');

Route::prefix('accounts')->group(function () {
    Route::resource('customers', CustomerController::class);
    Route::resource('admins', AdminController::class);
});

Route::patch('users/{user}/activate', [UserController::class, 'activate'])->name('users.activate');

// feedback
Route::resource('feedback', FeedbackController::class)->only('index', 'show', 'destroy');

// Settings
Route::get('settings', [SettingController::class, 'index'])->name('settings.index');
Route::put('settings', [SettingController::class, 'update'])->name('settings.update');

// profile
Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
Route::put('profile/{user}', [ProfileController::class, 'update'])->name('profile.update');

//reports
Route::get('reports', [ReportController::class, 'index'])->name('reports.index');

Route::resource('projects', ProjectController::class);

Route::resource('blogs', BlogController::class);