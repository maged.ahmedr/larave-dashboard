<x-layout :title="trans('feedback.plural')" :breadcrumbs="['dashboard.feedback.index']">
    @component('dashboard::components.table-box')

        @slot('title', trans('feedback.actions.list'))

        <tr>
            <th>@lang('feedback.attributes.title')</th>
            <th>@lang('feedback.attributes.message')</th>
            <th>@lang('feedback.attributes.created_at')</th>
            <th>...</th>
        </tr>
        @foreach($feedbacks as $feedback)
            <tr>
                <td>
                    <a href="{{ route('dashboard.feedback.show', $feedback) }}">
                        @if(! $feedback->isReaded())
                            <div class="badge badge-danger">@lang('feedback.new')</div>
                        @endif
                        &nbsp;
                        {{ $feedback->title }}
                    </a>
                </td>
                <td>{{ $feedback->message }}</td>
                <td>{{ $feedback->created_at->diffForHumans() }}</td>
                <td>
                    @include('dashboard.feedback.partials.actions.show')
                    @include('dashboard.feedback.partials.actions.delete')
                </td>
            </tr>
        @endforeach

        @if($feedbacks->hasPages())
            @slot('footer')
                {{ $feedbacks->links() }}
            @endslot
        @endif
    @endcomponent
</x-layout>