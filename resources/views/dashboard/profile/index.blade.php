<x-layout :title="trans('profiles.plural')" :breadcrumbs="['dashboard.profiles.index']">
{{ BsForm::resource($resourceType . 's')->put(route('dashboard.profile.update', $user->id)) }}
    @component('dashboard::components.box')
        {{ BsForm::text('name')->value($user->name) }}
        {{ BsForm::text('email')->value($user->email) }}
        {{ BsForm::text('phone')->value($user->phone) }}
        {{ BsForm::password('password') }}
        {{ BsForm::password('password_confirmation') }}

        {{ BsForm::image('avatar')->collection('avatars')->files($user->getMediaResource('avatars')) }}

        @slot('footer')
            {{ BsForm::submit()->label(trans('customers.actions.save')) }}
        @endslot
    @endcomponent
{{ BsForm::close() }}
</x-layout>