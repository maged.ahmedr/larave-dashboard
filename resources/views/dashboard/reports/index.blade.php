<x-layout :title="trans('reports.plural')" :breadcrumbs="['dashboard.reports.index']">

@include('dashboard.reports.reservations_count')

@include('dashboard.reports.users_by_place')

</x-layout>


@prepend('scripts')
<script>
    $(function() {
        /** Get Cities related to the chosen country  */
        $('select[name=country_id]').change(function() {
            $.ajax({
                url: "/api/cities",
                data: {
                    country_id: $(this).val()
                },
                success: function(data) {
                    var select = $('form select[name= city_id]');
                    $('.cities').removeClass('d-none');
                    select.empty();

                    $.each(data,function(key, value) {
                        select.append('<option value=' + key + '>' + value + '</option>');
                    });
                }
            });
        });
        
        /** Get Areas related to the chosen city  */
        $('select[name=city_id]').change(function() {
            $.ajax({
                url: "/api/areas",
                data: {
                    city_id: $(this).val()
                },
                success: function(data) {
                    var select = $('form select[name= area_id]');
                    $('.areas').removeClass('d-none');
                    select.empty();

                    $.each(data,function(key, value) {
                        select.append('<option value=' + key + '>' + value + '</option>');
                    });
                }
            });
        });


    });
</script>
@endprepend
