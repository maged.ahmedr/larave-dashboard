@can('create', Admin::class)
<div class="card card-default">
    <div class="card-header">
        <h3 class="card-title">{{ trans('reports.users_by_place')}}</h3>
    </div>

    <form>
        <div class="card-body {{ $bodyClass ?? '' }}">
            <div class="row">
                <div class="col-md-2">
                    {{ BsForm::select('country')->name('country_id')->options($countries)->placeholder('Select')->label(trans('customers.attributes.country'))->value('country_id') }}
                </div>
                <div class="col-md-2">
                    {{ BsForm::select('city')->name('city_id')->options($cities)->placeholder('Select')->label(trans('customers.attributes.city'))->value('city_id') }}
                </div>
                <div class="col-md-2">
                    {{ BsForm::select('area')->name('area_id')->options($areas)->placeholder('Select')->label(trans('customers.attributes.area'))->value('area_id') }}
                </div>
                <div class="col-md-3">
                    {{ BsForm::select('type')->name('type')->options($types)->placeholder('Select')->label(trans('reports.type')) }}
                </div>
                <div class="col-md-3">
                    @include('dashboard::components.info-box', [
                        'icon_color' => 'red',
                        'icon' => 'fa fa-users',
                        'text' => trans('reports.count'),
                        'number' => number_format($usersCount),
                    ])
                </div>
            </div>
        </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fas fa fa-fw fa-filter"></i>
                    @lang('reservations.actions.filter')
                </button>
            </div>
        </div>
    </form>
</div>
@endcan
