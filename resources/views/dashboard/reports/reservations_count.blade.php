@push('styles')

<style>
    .table-condensed  > th {
        font-size: 10px;
    }
</style>
@endpush
<div class="card card-default">
    <div class="card-header">
        <h3 class="card-title">{{ trans('reports.specialization_reservations')}}</h3>
    </div>

    <form>
        <div class="card-body {{ $bodyClass ?? '' }}">
            <div class="row">
                <div class="col-md-3">
                    <label for="from">@lang('reservations.attributes.range')</label>
                    <input type="datetime" name="range" class="form-control date">
                </div>
                <div class="col-md-3">
                    {{ BsForm::select('specialization')->options($specializations)->value(request('specialization'))->label(trans('reservations.attributes.specialization')) }}
                </div>
                <div class="col-md-3">
                    @include('dashboard::components.info-box', [
                        'icon_color' => 'green',
                        'icon' => 'fa fa-book',
                        'text' => trans('reservations.plural'),
                        'number' => number_format($timedReservations),
                    ])
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fas fa fa-fw fa-filter"></i>
                @lang('reservations.actions.filter')
            </button>
        </div>
    </form>
</div>

@push('scripts')
<script>
    $('.date').daterangepicker({
        showDropdowns: true,
        "locale": {
            "format": "YYYY/MM/DD",
            "applyLabel": "تأكيد",
            "cancelLabel": "إلغاء",
            "fromLabel": "من",
            "toLabel": "إلي",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "أحد",
                "إثنين",
                "ثلاثاء",
                "أربعاء",
                "خميس",
                "جمعه",
                "سبت"
            ],
            "monthNames": [
                "كانون الثانى",
                "شباط",
                "آذار",
                "نيسان",
                "أيار",
                "حزيران",
                "تموز",
                "آب",
                "أيلول",
                "تشرين الأول",
                "تشرين الثاني",
                "كانون الأول"
            ],
            "firstDay": 1
        },
    });
</script>
@endpush